import os
import zipfile
import time
import random

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

PROXY_HOST = 'node-ru-18.astroproxy.com'
PROXY_PORT = 10315
PROXY_USER = 'pplogin77'
PROXY_PASS = 'pppass77'


manifest_json = """
{
    "version": "1.0.0",
    "manifest_version": 2,
    "name": "Chrome Proxy",
    "permissions": [
        "proxy",
        "tabs",
        "unlimitedStorage",
        "storage",
        "<all_urls>",
        "webRequest",
        "webRequestBlocking"
    ],
    "background": {
        "scripts": ["background.js"]
    },
    "minimum_chrome_version":"22.0.0"
}
"""

background_js = """
var config = {
        mode: "fixed_servers",
        rules: {
          singleProxy: {
            scheme: "http",
            host: "%s",
            port: parseInt(%s)
          },
          bypassList: ["localhost"]
        }
      };

chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

function callbackFn(details) {
    return {
        authCredentials: {
            username: "%s",
            password: "%s"
        }
    };
}

chrome.webRequest.onAuthRequired.addListener(
            callbackFn,
            {urls: ["<all_urls>"]},
            ['blocking']
);
""" % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)


def get_chromedriver(use_proxy=False, user_agent=None):
    path = os.path.dirname(os.path.abspath(__file__))
    chrome_options = webdriver.ChromeOptions()

    preferences = {
        "webrtc.ip_handling_policy" : "disable_non_proxied_udp",
        "webrtc.multiple_routes_enabled": False,
        "webrtc.nonproxied_udp_enabled" : False
    }
    chrome_options.add_experimental_option("prefs", preferences)
    
    if use_proxy:
        pluginfile = 'proxy_auth_plugin.zipi'

        with zipfile.ZipFile(pluginfile, 'w') as zp:
            zp.writestr("manifest.json", manifest_json)
            zp.writestr("background.js", background_js)
        chrome_options.add_extension(pluginfile)
    if user_agent:
        chrome_options.add_argument('--user-agent=%s' % user_agent)
    driver = webdriver.Chrome(options=chrome_options)
    return driver

def main():
    driver = get_chromedriver(use_proxy=True, user_agent='Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36')
    
    driver.get('https://www.paypal.com/signin/')

    elem = driver.find_element_by_id('email')
    email = "email"
    for character in email:
        elem.send_keys(character)
        time.sleep(random.random())
        
    driver.implicitly_wait(150)
    elem.send_keys(Keys.RETURN)

    driver.implicitly_wait(120)
    elem = driver.find_element_by_id('password')
    password = "password"
    for character in password:
        elem.send_keys(character)
        time.sleep(random.random())

    driver.implicitly_wait(130)
    elem.send_keys(Keys.RETURN)

    driver.implicitly_wait(150)
    elem = driver.find_element_by_id('notNowLink')
    elem.click()

    driver.implicitly_wait(150)

    driver.get('https://business.paypal.com/merchantdata/reportHome?reportType=DLOG')
    elem = driver.find_element_by_id('download_0')
    elem.click()

if __name__ == '__main__':
    main()

