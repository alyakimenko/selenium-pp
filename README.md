### Инструкция

**Установка Selenium**

`pip install selenium`

**Установка драйвера**, в данном случае - Chrome.

Ссылки на драйверы различных браузеров:

- Chrome: 	https://sites.google.com/a/chromium.org/chromedriver/downloads
- Edge: 	https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
- Firefox: 	https://github.com/mozilla/geckodriver/releases
- Safari: 	https://webkit.org/blog/6900/webdriver-support-in-safari-10/

После скачивания, сделать unzip и перенести бинарник драйвера в `/usr/bin` или `/usr/local/bin`.